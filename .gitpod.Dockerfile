FROM gitpod/workspace-full

# Install custom tools, runtime, etc.
RUN sudo apt-get update && \ 
  sudo apt-get install -y && \
  sudo rm -rf /var/lib/apt/lists/*

# Apply user-specific settings
ENV PATH "${PATH}:${HOME}/bin"

# Install system dev tools
RUN npm i -g npm yarn prettier eslint pnpm \
  ts-node @teambit/bvm nx strapi prisma pm2
RUN bvm install
RUN bit config set anonymous_reporting true && \
  bit config set analytics_reporting true && \
  bit config set error_reporting true && \
  bit config set no_warnings false

## just some convienience aliases
RUN echo "alias y='yarn'" >> ~/.bashrc && \
  echo "alias x='yarn nx'" >> ~/.bashrc && \
  echo "alias b='/home/gitpod/bin/bit'" >> ~/.bashrc && \
  echo "alias s='yarn stencil'" >> ~/.bashrc