import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"
import AboutUs from "../views/examples/AboutUs"
import BlogPost from "../views/examples/BlogPost"
import BlogPosts from "../views/examples/BlogPosts"
import ContactUs from "../views/examples/ContactUs"
import Ecommerce from "../views/examples/Ecommerce"
import LandingPage from "../views/examples/LandingPage"
import LoginPage from "../views/examples/LoginPage"
import Pricing from "../views/examples/Pricing"
import ProductPage from "../views/examples/ProductPage"
import ProfilePage from "../views/examples/ProfilePage"
import SignupPage from "../views/examples/SignupPage"
import Index from "../views/Index"
import NucleoIcons from "../views/NucleoIcons"
import Presentation from "../views/Presentation"
import Sections from "../views/Sections"

export function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact render={() => <LandingPage />} />
        <Route path="/about/" render={() => <AboutUs />} />
        <Route path="/blog/" render={() => <BlogPosts />} />
        <Route path="/contact/" render={() => <ContactUs />} />
        <Route path="/login-page/" render={() => <LoginPage />} />
        <Route path="/presentation/" render={() => <Presentation />} />
        <Route path="/sign-up/" render={() => <SignupPage />} />
        <Redirect to="/" />
      </Switch>
    </BrowserRouter>
  )
}

export default App
