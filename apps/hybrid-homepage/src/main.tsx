import { StrictMode } from "react"
import * as ReactDOM from "react-dom"
import App from "./app/app"
import "assets/css/bootstrap.min.css"
import "assets/demo/demo.css"
import "assets/demo/nucleo-icons-page-styles.css"
import "assets/demo/react-demo.css"
import "assets/scss/now-ui-kit.scss"

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById("root")
)
