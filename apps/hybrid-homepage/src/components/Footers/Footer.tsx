/*eslint-disable*/
import React from "react"
import { Link } from "react-router-dom"
// reactstrap components
import { Container } from "reactstrap"

// core components

function Footer() {
  return (
    <>
      <footer className="footer">
        <Container>
          <nav>
            <ul>
              <li>
                <Link to="/" target="_blank">
                  Watheia
                </Link>
              </li>
              <li>
                <Link to="/about-us" target="_blank">
                  About Us
                </Link>
              </li>
              <li>
                <Link to="/blog" target="_blank">
                  Blog
                </Link>
              </li>
            </ul>
          </nav>
          <div className="copyright" id="copyright">
            &copy; 2021
            <a href="https://watheia.app/" target="_blank">
              Watheia Labs, LLC.
            </a>{" "}
            All rights reserved.
          </div>
        </Container>
      </footer>
    </>
  )
}

export default Footer
