/*eslint-disable*/
import React from "react"
import { Link } from "react-router-dom"
// reactstrap components
import { Container } from "reactstrap"

// core components

function FooterDefault() {
  return (
    <>
      <footer className="footer footer-default">
        <Container>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
              <li>
                <Link to="/blog">Blog</Link>
              </li>
            </ul>
          </nav>
          <div className="copyright" id="copyright">
            &copy; 2021
            <a href="https://watheia.app/" target="_blank">
              Watheia Labs, LLC.
            </a>{" "}
            All rights reserved.
          </div>
        </Container>
      </footer>
    </>
  )
}

export default FooterDefault
