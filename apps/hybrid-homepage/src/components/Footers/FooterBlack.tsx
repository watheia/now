/*eslint-disable*/
import React from "react"
// reactstrap components
import { Container } from "reactstrap"

// core components

function FooterBlack() {
  return (
    <>
      <footer className="footer" data-background-color="black">
        <Container>
          <nav>
            <ul>
              <li>
                <a
                  href="https://watheia.app?ref=nuk-pro-react-footer-black"
                  target="_blank"
                >
                  Watheia
                </a>
              </li>
              <li>
                <a
                  href="http://presentation.creative-tim.com?ref=nuk-pro-react-footer-black"
                  target="_blank"
                >
                  About Us
                </a>
              </li>
              <li>
                <a
                  href="http://blog.creative-tim.com?ref=nuk-pro-react-footer-black"
                  target="_blank"
                >
                  Blog
                </a>
              </li>
            </ul>
          </nav>
          <div className="copyright" id="copyright">
            &copy; 2021
            <a href="https://watheia.app/" target="_blank">
              Watheia Labs, LLC.
            </a>{" "}
            All rights reserved.
          </div>
        </Container>
      </footer>
    </>
  )
}

export default FooterBlack
