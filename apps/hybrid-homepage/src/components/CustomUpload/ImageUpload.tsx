import defaultImage from "assets/img/image_placeholder.jpg"
import defaultAvatar from "assets/img/placeholder.jpg"
// used for making the prop types of this component
import PropTypes from "prop-types"
import React from "react"
import { Button } from "reactstrap"
import { invoke, set } from "lodash"

function ImageUpload(props) {
  const [file, setFile] = React.useState(null)
  const [imagePreviewUrl, setImagePreviewUrl] = React.useState<string | ArrayBuffer>(
    props.avatar ? defaultAvatar : defaultImage
  )
  const fileInput: any = React.createRef()
  const handleImageChange = (e) => {
    e.preventDefault()
    const reader = new FileReader()
    const file = e.target.files[0]
    reader.onloadend = () => {
      setFile(file)
      setImagePreviewUrl(reader.result)
    }
    if (file) {
      reader.readAsDataURL(file)
    }
  }
  // const handleSubmit = e => {
  // e.preventDefault();
  // this.state.file is the file/image uploaded
  // in this function you can save the image (this.state.file) on form submit
  // you have to call it yourself
  // };
  const handleClick = () => {
    invoke(fileInput.current, "click")
  }
  const handleRemove = () => {
    setFile(null)
    setImagePreviewUrl(props.avatar ? defaultAvatar : defaultImage)
    set(fileInput, "current.value", null)
  }
  return (
    <div className="fileinput text-center">
      <input type="file" onChange={handleImageChange} ref={fileInput} />
      <div
        className={
          "fileinput-new thumbnail img-raised" + (props.avatar ? " img-circle" : "")
        }
      >
        <img
          src={
            typeof imagePreviewUrl === "string"
              ? imagePreviewUrl
              : Buffer.from(imagePreviewUrl).toString()
          }
          alt="..."
        />
      </div>
      <div>
        {file === null ? (
          <Button className="btn-round" color="default" onClick={handleClick}>
            {props.avatar ? "Add Photo" : "Select image"}
          </Button>
        ) : (
          <span>
            <Button className="btn-round" color="default" onClick={handleClick}>
              Change
            </Button>
            {props.avatar ? <br /> : null}
            <Button color="danger" className="btn-round" onClick={handleRemove}>
              <i className="fa fa-times" /> Remove
            </Button>
          </span>
        )}
      </div>
    </div>
  )
}

ImageUpload.propTypes = {
  avatar: PropTypes.bool
}

export default ImageUpload
