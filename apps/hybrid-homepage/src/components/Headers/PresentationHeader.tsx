/*eslint-disable*/
import React from "react"

// reactstrap components

// core components

function PresentationHeader() {
  return (
    <>
      <div className="page-header clear-filter">
        <div className="rellax-header rellax-header-sky" data-rellax-speed="-4">
          <div
            className="page-header-image"
            style={{
              backgroundImage:
                "url(" +
                require("assets/img/presentation-page/nuk-pro-back-sky.jpg").default +
                ")"
            }}
          ></div>
        </div>
        <div className="rellax-header rellax-header-buildings" data-rellax-speed="0">
          <div
            className="page-header-image page-header-city"
            style={{
              backgroundImage:
                "url(" +
                require("assets/img/presentation-page/nuk-pro-buildings.png").default +
                ")"
            }}
          ></div>
        </div>
        <div className="rellax-text-container rellax-text">
          <h1 className="h1-seo" data-rellax-speed="-1">
            Watheia
          </h1>
          <div className="pro">NOW</div>
        </div>
        <h3 className="h3-description rellax-text" data-rellax-speed="-1">
          We help startups an companies build amaizing products.
        </h3>
      </div>
    </>
  )
}

export default PresentationHeader
