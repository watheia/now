import React from "react"

// reactstrap components

// core components

function ProductPageHeader() {
  const pageHeader = React.createRef<HTMLDivElement>()
  React.useEffect(() => {
    if (window.innerWidth > 991) {
      const updateScroll = () => {
        const windowScrollTop = window.pageYOffset / 3
        if (pageHeader.current)
          pageHeader.current.style.transform = "translate3d(0," + windowScrollTop + "px,0)"
        return undefined
      }
      window.addEventListener("scroll", updateScroll)
      return function cleanup() {
        window.removeEventListener("scroll", updateScroll)
      }
    }
  })
  return (
    <div className="page-header page-header-mini">
      <div
        className="page-header-image"
        style={{
          backgroundImage: "url(" + require("assets/img/pp-cov.jpg").default + ")"
        }}
        ref={pageHeader}
      ></div>
    </div>
  )
}

export default ProductPageHeader
