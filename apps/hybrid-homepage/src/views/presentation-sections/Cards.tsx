import React from "react"
// reactstrap components
import { Col, Container, Row } from "reactstrap"

// core components

function Cards() {
  return (
    <div className="section section-cards">
      <Container>
        <Row>
          <Col className="text-center ml-auto mr-auto" md="8">
            <div className="section-description">
              <h2 className="title">Engage your customers</h2>
              <h6 className="category">Generate natural engagement and raving fans</h6>
              <h5 className="description">
                Your customer experience is what makes your business unique from all others.
                So shouldn't usability be the critical performance metric of which to
                measure our work? Ask us how we imperially measure usability to provide
                objective scorings in line with your stated goals.
              </h5>
            </div>
          </Col>
        </Row>
        <Row>
          <Col md="12">
            <div className="images-container">
              <div className="image-container1 mr-1">
                <img
                  alt="..."
                  src={require("assets/img/presentation-page/card3.jpg").default}
                ></img>
              </div>
              <div className="image-container2 mr-1">
                <img
                  alt="..."
                  src={require("assets/img/presentation-page/card6.jpg").default}
                ></img>
              </div>
              <div className="image-container3 mr-1">
                <img
                  alt="..."
                  src={require("assets/img/presentation-page/card7.jpg").default}
                ></img>
              </div>
              <div className="image-container4 mr-1">
                <img
                  alt="..."
                  src={require("assets/img/presentation-page/card5.jpg").default}
                ></img>
              </div>
              <div className="image-container5">
                <img
                  alt="..."
                  src={require("assets/img/presentation-page/card4.jpg").default}
                ></img>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Cards
