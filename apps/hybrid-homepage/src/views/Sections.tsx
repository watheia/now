import React from "react"
import Footer from "../components/Footers/Footer"
import WhiteNavbar from "../components/Navbars/WhiteNavbar"
import Blogs from "./sections-sections/Blogs"
import ContactUs from "./sections-sections/ContactUs"
import Features from "./sections-sections/Features"
import Headers from "./sections-sections/Headers"
import Pricing from "./sections-sections/Pricing"
import Projects from "./sections-sections/Projects"
import Teams from "./sections-sections/Teams"
import Testimonials from "./sections-sections/Testimonials"

function Sections(): JSX.Element {
  React.useEffect(() => {
    document.body.classList.add("sections-page")
    document.body.classList.add("sidebar-collapse")
    document.documentElement.classList.remove("nav-open")
    const href = window.location.href.substring(window.location.href.lastIndexOf("#/") + 2)
    const hrefId = href.substring(href.lastIndexOf("#") + 1)
    if (href.lastIndexOf("#") > 0) {
      document.getElementById(hrefId)?.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "nearest"
      })
    }
    return function cleanup() {
      document.body.classList.remove("sections-page")
      document.body.classList.remove("sidebar-collapse")
    }
  })
  return (
    <>
      <WhiteNavbar />
      <div className="wrapper">
        <div className="section-space"></div>
        <Headers />
        <Features />
        <Blogs />
        <Teams />
        <Projects />
        <Pricing />
        <Testimonials />
        <ContactUs />
        <Footer />
      </div>
    </>
  )
}

export default Sections
