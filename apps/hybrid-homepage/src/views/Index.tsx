import FooterBlack from "components/Footers/FooterBlack"
import IndexHeader from "components/Headers/IndexHeader"
// reactstrap components
// core components
import ScrollTransparentNavbar from "components/Navbars/ScrollTransparentNavbar"
import React from "react"
import Basic from "./index-sections/Basic"
import Cards from "./index-sections/Cards"
import Carousel from "./index-sections/Carousel"
import ContentAreas from "./index-sections/ContentAreas"
import FileUploader from "./index-sections/FileUploader"
import Footers from "./index-sections/Footers"
// sections for this page
import Images from "./index-sections/Images"
import Javascript from "./index-sections/Javascript"
import Navbars from "./index-sections/Navbars"
import Notifications from "./index-sections/Notifications"
import NucleoIcons from "./index-sections/NucleoIcons"
import Pagination from "./index-sections/Pagination"
import Pills from "./index-sections/Pills"
import PlainCards from "./index-sections/PlainCards"
import PreFooter from "./index-sections/PreFooter"
import Tabs from "./index-sections/Tabs"
import Typography from "./index-sections/Typography"

function Index() {
  React.useEffect(() => {
    document.body.classList.add("index-page")
    document.body.classList.add("sidebar-collapse")
    document.documentElement.classList.remove("nav-open")
    window.scrollTo(0, 0)
    document.body.scrollTop = 0
    return function cleanup() {
      document.body.classList.remove("index-page")
      document.body.classList.remove("sidebar-collapse")
    }
  })
  return (
    <>
      <ScrollTransparentNavbar />
      <div className="wrapper">
        <IndexHeader />
        <div className="main">
          <Images />
          <Basic />
          <Navbars />
          <Tabs />
          <Pills />
          <Pagination />
          <Notifications />
          <PreFooter />
          <Footers />
          <Typography />
          <ContentAreas />
          <Cards />
          <PlainCards />
          <Javascript />
          <FileUploader />
          <Carousel />
          <NucleoIcons />
          <FooterBlack />
        </div>
      </div>
    </>
  )
}

export default Index
