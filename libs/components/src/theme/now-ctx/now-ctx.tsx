import { createContext } from 'react'

export type ColorScheme = "light" | "dark"

export type NowCtxType = {
  /**
   * default color of scheme.
   */
  colorScheme?: ColorScheme
}

export const NowCtx = createContext<NowCtxType>({
  colorScheme: undefined
})
