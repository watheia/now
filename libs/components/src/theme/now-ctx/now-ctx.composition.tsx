import React, { useContext } from "react"
import { NowProvider } from "./now-ctx.provider"
import { NowCtx } from "./now-ctx"

export function MockComponent() {
  const theme = useContext(NowCtx)

  return (
    <div data-testid="mock-component" style={{ color: theme.colorScheme }}>
      this should be {theme.colorScheme}
    </div>
  )
}

export const BasicThemeUsage = () => {
  return (
    <NowProvider colorScheme="dark">
      <MockComponent />
    </NowProvider>
  )
}
