import React from "react"
import { BasicThemeUsage } from './now-ctx.composition'
import { render } from '@testing-library/react'

it('should render the button in the color blue', () => {
  const { getByTestId } = render(<BasicThemeUsage />)
  const rendered = getByTestId('mock-component')
  expect(rendered).toBeTruthy()
})
