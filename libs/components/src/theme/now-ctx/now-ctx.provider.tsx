import React, { ReactNode } from "react"
import { NowCtx, ColorScheme } from "./now-ctx"

export type NowProviderProps = {
  /**
   * primary color of theme.
   */
  colorScheme?: ColorScheme

  /**
   * children to be rendered within this theme.
   */
  children: ReactNode
}

export function NowProvider({ colorScheme, children }: NowProviderProps) {
  return <NowCtx.Provider value={{ colorScheme }}>{children}</NowCtx.Provider>
}
