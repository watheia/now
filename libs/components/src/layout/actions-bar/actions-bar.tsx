import React from "react"
import { HTMLAttributes } from "react"
import { Link, NavLink } from "react-router-dom"
// reactstrap components
import {
  Collapse,
  Container,
  Nav,
  Navbar,
  NavbarBrand,
  NavItem,
  UncontrolledTooltip
} from "reactstrap"

export type ActionsBarProps = HTMLAttributes<HTMLDivElement>
export function ActionsBar(props: ActionsBarProps) {
  const [collapseOpen, setCollapseOpen] = React.useState(false)
  const [navbarColor, setNavbarColor] = React.useState(
    (document.documentElement.scrollTop > 499 || document.body.scrollTop) > 499
      ? ""
      : " navbar-transparent"
  )
  const [buyButtonColor, setBuyButtonColor] = React.useState(
    (document.documentElement.scrollTop > 499 || document.body.scrollTop) > 499
      ? "primary"
      : "neutral"
  )
  React.useEffect(() => {
    const updateNavbarColor = () => {
      if (document.documentElement.scrollTop > 499 || document.body.scrollTop > 499) {
        setNavbarColor("")
        setBuyButtonColor("primary")
      } else if (
        document.documentElement.scrollTop < 500 ||
        document.body.scrollTop < 500
      ) {
        setNavbarColor(" navbar-transparent")
        setBuyButtonColor("neutral")
      }
    }
    window.addEventListener("scroll", updateNavbarColor)
    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor)
    }
  })
  return (
    <>
      {collapseOpen ? (
        <div
          id="bodyClick"
          onClick={() => {
            document.documentElement.classList.toggle("nav-open")
            setCollapseOpen(false)
          }}
        />
      ) : null}
      <Navbar className={"fixed-top" + navbarColor} color="white" expand="lg">
        <Container>
          <div className="navbar-translate" {...props}>
            <NavbarBrand to="/" tag={Link} id="navbar-brand">
              Watheia
            </NavbarBrand>
            <UncontrolledTooltip target="navbar-brand">
              Watheia Labs, LLC
            </UncontrolledTooltip>
            <button
              onClick={() => {
                document.documentElement.classList.toggle("nav-open")
                setCollapseOpen(!collapseOpen)
              }}
              aria-expanded={collapseOpen}
              className="navbar-toggler"
            >
              <span className="navbar-toggler-bar top-bar"></span>
              <span className="navbar-toggler-bar middle-bar"></span>
              <span className="navbar-toggler-bar bottom-bar"></span>
            </button>
          </div>
          <Collapse isOpen={collapseOpen} navbar>
            <Nav className="ml-auto" id="ceva" navbar>
              <NavItem>
                <NavLink className="nav-link" to="/">
                  <p>Home</p>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="nav-link" to="/about">
                  <p>About</p>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink className="nav-link" to="/blog">
                  <p>Blog</p>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className="nav-link btn-default"
                  color={buyButtonColor}
                  to="/support"
                >
                  <p>Get Support</p>
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    </>
  )
}

export default ActionsBar
