import { render } from "@testing-library/react"

import ActionsBar from "./actions-bar"

describe("ActionsBar", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<ActionsBar />)
    expect(baseElement).toBeTruthy()
  })
})
