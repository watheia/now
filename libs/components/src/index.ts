export { AboutHero } from "./sections/about-hero"
export type { AboutHeroProps } from "./sections/about-hero"

export { SupportHero } from "./sections/support-hero"
export type { SupportHeroProps } from "./sections/support-hero"

export { ContactSection } from "./sections/contact-section"
export type { ContactSectionProps } from "./sections/contact-section"

export { default, HomepageHero } from "./sections/homeage-hero"
export type { HomepageHeroProps } from "./sections/homeage-hero"

export { Footer } from "./layout/footer/footer"
export type { FooterProps } from "./layout/footer"

export { ActionsBar } from "./layout/actions-bar"
export type { ActionsBarProps } from "./layout/actions-bar"

export { NowCtx, NowProvider } from "./theme/now-ctx"
export type { NowProviderProps } from "./theme/now-ctx"
