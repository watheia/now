/* eslint-disable @typescript-eslint/no-var-requires */
import React, { HtmlHTMLAttributes } from "react"
// reactstrap components
import { Button, Container } from "reactstrap"

// core components

export type HomepageHeroProps = HtmlHTMLAttributes<HTMLDivElement>

export function HomepageHero(props: HomepageHeroProps) {
  const pageHeader = React.createRef<HTMLDivElement>()
  React.useEffect(() => {
    if (window.innerWidth > 991) {
      const updateScroll = () => {
        const windowScrollTop = window.pageYOffset / 3
        if (pageHeader.current)
          pageHeader.current.style.transform = "translate3d(0," + windowScrollTop + "px,0)"

        return () => {
          window.removeEventListener("scroll", updateScroll)
        }
      }
      return () => {
        window.removeEventListener("scroll", updateScroll)
      }
    }
  })
  return (
    <div className="page-header page-header-small" {...props}>
      <div
        className="page-header-image"
        style={{
          backgroundImage: "url(../../assets/img/bg26.jpg)"
        }}
        ref={pageHeader}
      ></div>
      <div className="content-center">
        <Container>
          <h1 className="title">
            We build amaizing digital products for startups and companies.
          </h1>
          <div className="text-center">
            <Button
              className="btn-icon btn-round mr-1"
              color="info"
              href="#watheia"
              onClick={(e) => e.preventDefault()}
            >
              <i className="fab fa-facebook-square"></i>
            </Button>
            <Button
              className="btn-icon btn-round mr-1"
              color="info"
              href="#watheia"
              onClick={(e) => e.preventDefault()}
            >
              <i className="fab fa-twitter"></i>
            </Button>
            <Button
              className="btn-icon btn-round"
              color="info"
              href="#watheia"
              onClick={(e) => e.preventDefault()}
            >
              <i className="fab fa-google-plus"></i>
            </Button>
          </div>
        </Container>
      </div>
    </div>
  )
}

export default HomepageHero
