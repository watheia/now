import "./about-hero.module.scss"

/* eslint-disable-next-line */
export interface AboutHeroProps {}

export function AboutHero(props: AboutHeroProps) {
  return (
    <div>
      <h1>Welcome to AboutHero!</h1>
    </div>
  )
}

export default AboutHero
