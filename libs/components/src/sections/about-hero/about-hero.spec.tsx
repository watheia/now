import { render } from "@testing-library/react"

import AboutHero from "./about-hero"

describe("AboutHero", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<AboutHero />)
    expect(baseElement).toBeTruthy()
  })
})
