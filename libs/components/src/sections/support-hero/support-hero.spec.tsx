import { render } from "@testing-library/react"

import SupportHero from "./support-hero"

describe("SupportHero", () => {
  it("should render successfully", () => {
    const { baseElement } = render(<SupportHero />)
    expect(baseElement).toBeTruthy()
  })
})
