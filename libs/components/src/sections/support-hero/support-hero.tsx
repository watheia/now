import "./support-hero.module.scss"

/* eslint-disable-next-line */
export interface SupportHeroProps {}

export function SupportHero(props: SupportHeroProps) {
  return (
    <div>
      <h1>Welcome to SupportHero!</h1>
    </div>
  )
}

export default SupportHero
