import "./content.module.scss"

/* eslint-disable-next-line */
export interface ContentProps {}

export function Content(props: ContentProps) {
  return (
    <div>
      <h1>Welcome to Content!</h1>
    </div>
  )
}

export default Content
